# blockade-detection

Code to detect and measure properties of ionic current blockades from a current vs time recording. 
Developed by Hadjer Ouldali and Fabien Piguet at the Oukhaled Lab, University of Cergy-Pontoise, France